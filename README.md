# folder2tumblr

**Posts images in folder to tumblr**. Has an option to watch a folder for image additions.

Includes PyTumblr `interactive_console.py` to generate credential files (with some tweaks).

Dependencies:

```
docopt
PyTumblr
PyYAML
watchdog
```

## Installation

```bash
python3 -m venv venv
source ./env/bin/activate
pip install docopt pyYAML pyTumblr watchdog

# Generate credentials:
# 1. Get your consumer_key, consumer_secret from tumblr
# 2. Run this script
python interactive_console.py
```

## Usage

```
Folder to tumblr

Usage:
  folder2tumblr.py [options] BLOGNAME FOLDER JSONFILE [POSTSTATE]
  folder2tumblr.py -h | --help

Arguments:
    BLOGNAME        Blogname
    FOLDER=DIR      Folder
    JSONFILE=FILE   json file name
    POSTSTATE       the post state 'queue' or 'published' [default: published]

Options:
    -w --watch      Watch folder
```
